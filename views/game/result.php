<?php
use yii\grid\GridView;

?>
Your score is <?= round($score, 2) ?> %
<?= GridView::widget([
    'id' => 'table',
    'dataProvider' => $dataProvider,
    'showFooter' => true,
    'columns' => [
        [
            'class' => 'yii\grid\SerialColumn',
            'contentOptions' => ['style' => 'width:50px;']
        ],
        'task',
        'result',
        'user_result',
    ],
]); ?>
