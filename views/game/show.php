<?php
use yii\helpers\Html;
use yii\helpers\Url;

?>
<?= $model->task . ' = ' . $model->user_result ?>
<?php
if ($model->result != $model->user_result) {
    ?>
    <a class="glyphicon glyphicon-remove"></a> INCORRECT
<?php
} else {
    ?>
    <a class="glyphicon glyphicon-ok"></a> CORRECT
<?php
}
?>
<br> Correct result is : <?= $model->result ?>
<br>
<br>
<?= Html::a('Next', Url::to([
    ($step >= (Yii::$app->params["maxStepCount"] + 1)) ? 'game/result' : 'game/index'
]), [
    'class' => 'btn btn-sm btn-success'
]) ?>
