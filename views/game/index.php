<?php
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\bootstrap\Html;


/* @var $this yii\web\View */

$this->title = 'Math game';
?>
    <div class="row">
        <div class="col-md-6">
            <h1>Simple math game</h1>
        </div>
        <div class="col-md-6"><h1>Step <?= $step ?> of <?= Yii::$app->params["maxStepCount"] ?></h1></div>
    </div>
    <div class="row ajax-settings">
        <div class="col-md-6">Use Ajax <?= Html::checkbox('is_ajax', false, ['id' => 'is_ajax']) ?> </div>
    </div>
    <div class="body-content">
        <?php $form = ActiveForm::begin([
            'action' => Url::to(['game/calculate']),
            'id' => 'submitForm',
            'options' => [
                'class' => 'form-inline',
            ]
        ]); ?>
        <div class="row question">
            <div class="col-md-6">
                <div class="col-md-6"><?= $model->a ?> <?= $model->operation ?> <?= $model->b ?> = ?</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="col-md-6">
                    <?= $form->field($model, 'user_result')
                        ->textInput()->label(false); ?>
                </div>
                <div class="col-md-6">
                    <?= Html::submitButton('<span class="glyphicon glyphicon-ok-circle"></span> Submit', [
                        'id' => 'btn_submit',
                        'class' => 'btn btn-success'
                    ]) ?>
                </div>
            </div>
        </div>
        <?= $form->field($model, 'a')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'operation')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'b')->hiddenInput()->label(false) ?>
        <?php ActiveForm::end(); ?>
        <div class="row">
            <div class="col-md-6">
                <div class="col-md-6" id="answer"></div>
            </div>
        </div>
    </div>
<?php
$js = <<<JS

        //$("#submitForm").submit(function (event) {
 $(document).on('beforeSubmit', '#submitForm', function (event, messages, errorAttributes) {
    //if Ajax flag set to on
    if ($("#is_ajax").is(':checked')) {
        $.ajax({
               type: "POST",
               url: $("#submitForm").attr('action')+'&isAjax=1',
               data: $("#submitForm").serialize(),
               success: function(data)
               {
                   $("#answer").html(data);
                   $("#btn_submit").attr("disabled","disabled");
               }
        });

        return false;
    }
  });
JS;

$this->registerJs($js);
?>