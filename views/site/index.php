<?php
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Simple math game</h1>
        <p><a class="btn btn-lg btn-success" href="<?= Url::to(['/game']) ?>">Start game</a></p>
    </div>
    <div class="body-content">
    </div>
</div>
