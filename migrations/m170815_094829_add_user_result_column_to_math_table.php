<?php

use yii\db\Migration;

/**
 * Handles adding user_result to table `math`.
 */
class m170815_094829_add_user_result_column_to_math_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('math', 'user_result', 'numeric(6,1)');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('math', 'user_result');
    }
}
