<?php

use yii\db\Migration;

class m170815_094506_change_result_column_in_math_table extends Migration
{

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->alterColumn('math', 'result', 'numeric(6,1)');
    }

    public function down()
    {
        $this->alterColumn('math', 'result', 'int');
    }

}
