<?php
namespace app\components;

use Yii;

class GeneratorHelper
{

    /**
     * return random operation
     * */
    public static function getOperation()
    {
        $operations = array('-', '+');
        return $operations[rand(0, 1)];
    }

    /**
     * return random number
     * */
    public static function getNumber()
    {
        return rand(10, 1000) / 10;
    }
}