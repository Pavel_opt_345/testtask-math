<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use app\components\GeneratorHelper;
use achertovsky\math\models\Math;
use yii\data\ActiveDataProvider;

class GameController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Displays Game homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $session = Yii::$app->session;
        if ($session->has('count')) {
            $step = $session->get('count');
            //if current step > maxStepCount
            if ($step > Yii::$app->params["maxStepCount"]) {
                return $this->redirect(['result']);
            }
        } else {
            $step = 1;
            $session->set('count', $step);
            //clear all previous results
            Math::deleteAll();
        }

        $model = new Math();
        $model->a = GeneratorHelper::getNumber();
        $model->b = GeneratorHelper::getNumber();
        $model->operation = GeneratorHelper::getOperation();

        return $this->render('index', [
            'model' => $model,
            'step' => $step
        ]);
    }

    /**
     * Calculate action.
     * @param integer $isAjax , type of request
     * @return Response|string
     */
    public function actionCalculate($isAjax = null)
    {
        $model = new Math();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->operation == "+") {
                $model->add();
            }
            if ($model->operation == "-") {
                $model->sub();
            }
            if ($model->validate()) {
                $session = Yii::$app->session;
                $step = $session->get('count') + 1;
                $session->set('count', $step);
                if ($isAjax != 1) {
                    return $this->redirect(['show', 'id' => $model->id]);
                } else {
                    $this->layout = false;
                    return $this->render('show', [
                        'model' => $model,
                        'step' => $step
                    ]);
                }
            }
        }

    }


    /**
     * Show action.
     * @param integer $id Math model
     * @return Response|string
     */
    public function actionShow($id)
    {
        $model = Math::findOne($id);
        $session = Yii::$app->session;
        $step = $session->get('count');

        return $this->render('show', [
            'model' => $model,
            'step' => $step
        ]);
    }

    /**
     * Result action.
     *
     * @return Response|string
     */
    public function actionResult()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Math::find()
        ]);

        $score = Math::getScore();
        return $this->render('result', [
            'dataProvider' => $dataProvider,
            'score' => $score
        ]);
    }

}
